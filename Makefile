CXXFLAGS = -Wall -Werror

SRCS := $(wildcard src/*.cpp)

OBJS := $(SRCS:.cpp=.o)

TARGET = example

$(TARGET): $(OBJS)
	$(CXX) $(CXXFLAGS) $(OBJS) -o $@

%.o: %.cpp $(wildcard src/*.h)
	$(CXX) $(CXXFLAGS) -c $< -o $@

.PHONY: clean

clean:
	rm -f $(OBJS) $(TARGET)
